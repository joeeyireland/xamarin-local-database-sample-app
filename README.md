# Xamarin Local Database Sample App

Xamarin Forms app to create a basic local database for adding, editing and deleting Companies
(using SQLite and Xamarin Forms)

# How To Open File

Simply clone the repo into any folder of your choice and opne the .SLN using Visual Studio
(Make sure you have Xamarin Studio VS Tools Installed)

# Updates & Added Features

I will continue to update this application and add new stuff to it so watch this Repo for updates if you would like to improve your current build of this sample.

Sample App Developed By: https://www.youtube.com/watch?v=aabHAgY5VXo&lc=z22xcfvbvuz0zljxv04t1aokgrwdqynbzkveynpwgg1wbk0h00410.1548155410023542
Updates / Fixes and Modified By Joey Ireland.