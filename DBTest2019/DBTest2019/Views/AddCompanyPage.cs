﻿using DBTest2019.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace DBTest2019.Views
{
    public class AddCompanyPage : ContentPage
    {

        private Entry _nameEntry;
        private Entry _AddressEntry;
        private Button _SaveButton;

        string _dbPath = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "myAppDB.db");

        public AddCompanyPage()
        {
            this.Title = "Add Company";

            StackLayout stacklayout = new StackLayout();

            _nameEntry = new Entry();
            _nameEntry.Keyboard = Keyboard.Text;
            _nameEntry.Placeholder = "Company Name";
            stacklayout.Children.Add(_nameEntry);

            _AddressEntry = new Entry();
            _AddressEntry.Keyboard = Keyboard.Text;
            _AddressEntry.Placeholder = "Company Address";
            stacklayout.Children.Add(_AddressEntry);

            _SaveButton = new Button();
            _SaveButton.Text = "Add";
            _SaveButton.Clicked += _SaveButton_Clicked;
            stacklayout.Children.Add(_SaveButton);

            Content = stacklayout;
        }

        private async void _SaveButton_Clicked(object sender, EventArgs e)
        {
            var db = new SQLiteConnection(_dbPath);
            db.CreateTable<Company>();

            var maxPK = db.Table<Company>().OrderByDescending(c => c.Id).FirstOrDefault();

            Company company = new Company()
            {
                Id = (maxPK == null ? 1 : maxPK.Id + 1),
                Name = _nameEntry.Text,
                Address = _AddressEntry.Text
            };
            db.Insert(company);
            await DisplayAlert(null, company.Name + " Saved", "Ok");
            await Navigation.PopAsync();
        }
    }
}