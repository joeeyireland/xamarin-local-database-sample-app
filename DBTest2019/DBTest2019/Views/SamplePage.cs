﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace DBTest2019.Views
{
    public class SamplePage : ContentPage
    {

        private Button _button;
        
        public SamplePage()
        {

            this.Title = "Sample Page";

            StackLayout stacklayout = new StackLayout();
            _button = new Button();
            _button.Text = "Alert 1";
            _button.Clicked += _button_Clicked;
            stacklayout.Children.Add(_button);

            _button = new Button();
            _button.Text = "Alert 2";
            _button.Clicked += _button1_Clicked;
            stacklayout.Children.Add(_button);

            Content = stacklayout;

        }
        private async void _button_Clicked(object sender, EventArgs e)
        {
            await DisplayAlert("Alert 1","Saved Successfully","Ok");
        }

        private async void _button1_Clicked(object sender, EventArgs e)
        {
            await DisplayAlert("Alert 2","Confirm Delete?","Confirm", "Cancel");
        }
    }
}