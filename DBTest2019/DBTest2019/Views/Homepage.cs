﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace DBTest2019.Views
{
    public class HomePage : ContentPage
    {
        public HomePage()
        {

            this.Title = "Select Option";

            StackLayout stacklayout = new StackLayout();
            Button button = new Button();
            button.Text = "Add Company";
            button.Clicked += ButtonClicked;
            stacklayout.Children.Add(button);

            button = new Button();
            button.Text = "Get From Database";
            button.Clicked += Button_Get_Clicked;
            stacklayout.Children.Add(button);

            button = new Button();
            button.Text = "Edit Companies";
            button.Clicked += Button_Edit_Clicked;
            stacklayout.Children.Add(button);

            button = new Button();
            button.Text = "Delete Company";
            button.Clicked += Button_Delete_Clicked;
            stacklayout.Children.Add(button);

            Content = stacklayout;
        }

        private async void ButtonClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new AddCompanyPage());
        }

        private async void Button_Get_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new GetAllCompaniesPage());
        }

        private async void Button_Edit_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new EditCompanyPage());
        }

        private async void Button_Delete_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new DeleteCompanyPage());
        }
    }
}