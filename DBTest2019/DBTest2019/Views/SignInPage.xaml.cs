﻿using DBTest2019.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DBTest2019
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SignInPage : ContentPage
    {
        private Label headerLabel;
        private Entry EmailEntry;
        private Entry PasswordEntry;
        private Button SignInButton;

        public SignInPage()
        {
            InitializeComponent();

            StackLayout stacklayout = new StackLayout();

            headerLabel = new Label();
            headerLabel.Text = "Signin Page";
            headerLabel.FontAttributes = FontAttributes.Bold;
            headerLabel.Margin = new Thickness(10, 10, 10, 10);
            headerLabel.HorizontalOptions = LayoutOptions.StartAndExpand;
            stacklayout.Children.Add(headerLabel);

            EmailEntry = new Entry();
            EmailEntry.Keyboard = Keyboard.Email;
            EmailEntry.Placeholder = "Enter Email Address...";
            stacklayout.Children.Add(EmailEntry);

            PasswordEntry = new Entry();
            PasswordEntry.Keyboard = Keyboard.Text;
            PasswordEntry.Placeholder = "Password";
            PasswordEntry.IsPassword = true;
            stacklayout.Children.Add(PasswordEntry);

            SignInButton = new Button();
            SignInButton.Text = "Sign In";
            SignInButton.Clicked += SigninButton_Clicked;
            stacklayout.Children.Add(SignInButton);

            Content = stacklayout;
        }

        private void SigninButton_Clicked(object sender, EventArgs e)
        {
            string email = EmailEntry.Text;
            string password = PasswordEntry.Text;

            Application.Current.MainPage = new NavigationPage(new HomePage());
        }
    }
}