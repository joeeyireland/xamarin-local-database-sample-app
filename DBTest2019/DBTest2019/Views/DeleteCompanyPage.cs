﻿using DBTest2019.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace DBTest2019.Views
{
    public class DeleteCompanyPage : ContentPage
    {
        private ListView _listView;
        private Button _Button;

        Company _Company = new Company();
        string _dbPath = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "myAppDB.db");

        public DeleteCompanyPage()
        {
            this.Title = "Delete Company";

            var db = new SQLiteConnection(_dbPath);

            StackLayout stacklayout = new StackLayout();

            _listView = new ListView
            {
                ItemTemplate = new DataTemplate(() =>
                {
                    Label nameLabel = new Label();
                    nameLabel.SetBinding(Label.TextProperty, "Name");
                    nameLabel.FontSize = 20;
                    Label addressLabel = new Label();
                    addressLabel.SetBinding(Label.TextProperty, "Address");

                    return new ViewCell
                    {
                        View = new StackLayout
                        {
                            Padding = new Thickness(20, 5),
                            Orientation = StackOrientation.Horizontal,
                            Children =
                            {
                                new StackLayout
                                {
                                    VerticalOptions = LayoutOptions.Center,
                                    Spacing = 0,
                                    Children =
                                    {
                                        nameLabel,
                                        addressLabel,
                                    }
                                }
                            }
                        }
                    };
                })
            };
            _listView.HasUnevenRows = true;
            _listView.ItemsSource = db.Table<Company>().OrderBy(x => x.Name).ToList();
            _listView.ItemSelected += _listView_ItemSelected;
            stacklayout.Children.Add(_listView);

            _Button = new Button();
            _Button.Text = "Delete";
            _Button.Clicked += _button_Clicked;
            stacklayout.Children.Add(_Button);

            Content = stacklayout;

        }

        private void _listView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            _Company = (Company)e.SelectedItem;
        }

        private async void _button_Clicked(object sender, EventArgs e)
        {
            var db = new SQLiteConnection(_dbPath);
            db.Table<Company>().Delete(x => x.Id == _Company.Id);
            await Navigation.PopAsync();
        }
    }
}