﻿using DBTest2019.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace DBTest2019.Views
{
    public class EditCompanyPage : ContentPage
    {

        private ListView _listView;
        private Entry _idEntry;
        private Entry _NameEntry;
        private Entry _AddressEntry;
        private Button _Button;

        Company _Company = new Company();
        string _dbPath = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "myAppDB.db");
        public EditCompanyPage()
        {
            this.Title = "Edit Company";

            var db = new SQLiteConnection(_dbPath);

            StackLayout stacklayout = new StackLayout();

            _listView = new ListView
            {
                ItemTemplate = new DataTemplate(() =>
                {
                    Label nameLabel = new Label();
                    nameLabel.SetBinding(Label.TextProperty, "Name");
                    nameLabel.FontSize = 20;
                    Label addressLabel = new Label();
                    addressLabel.SetBinding(Label.TextProperty, "Address");

                    return new ViewCell
                    {
                        View = new StackLayout
                        {
                            Padding = new Thickness(20, 5),
                            Orientation = StackOrientation.Horizontal,
                            Children =
                            {

                                new StackLayout
                                {
                                    VerticalOptions = LayoutOptions.Center,
                                    Spacing = 0,
                                    Children =
                                    {
                                        nameLabel,
                                        addressLabel,
                                    }
                                }
                            }
                        }
                    };
                })
            };
            _listView.HasUnevenRows = true;
            _listView.ItemsSource = db.Table<Company>().OrderBy(x => x.Name).ToList();
            _listView.ItemSelected += _listView_ItemSelected;
            stacklayout.Children.Add(_listView);

            _idEntry = new Entry();
            _idEntry.Placeholder = "ID";
            _idEntry.IsVisible = false;
            stacklayout.Children.Add(_idEntry);

            _NameEntry = new Entry();
            _NameEntry.Keyboard = Keyboard.Text;
            _NameEntry.Placeholder = "Company Name";
            stacklayout.Children.Add(_NameEntry);

            _AddressEntry = new Entry();
            _AddressEntry.Keyboard = Keyboard.Text;
            _AddressEntry.Placeholder = "Company Address";
            stacklayout.Children.Add(_AddressEntry);

            _Button = new Button();
            _Button.Text = "Update Company";
            _Button.Clicked += _button_Clicked;
            stacklayout.Children.Add(_Button);

            Content = stacklayout;


        }

        private async void _button_Clicked(object sender, EventArgs e)
        {
            var db = new SQLiteConnection(_dbPath);
            Company company = new Company()
            {
                Id = Convert.ToInt32(_idEntry.Text),
                Name = _NameEntry.Text,
                Address = _AddressEntry.Text
            };
            db.Update(company);
            await Navigation.PopAsync();
        }

        private void _listView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            _Company = (Company)e.SelectedItem;
            _idEntry.Text = _Company.Id.ToString();
            _NameEntry.Text = _Company.Name;
            _AddressEntry.Text = _Company.Address;
        }
    }
}